// Charts.tsx
/*
import React from 'react';
import { Line } from 'react-chartjs-2';

const Charts: React.FC = () => {
    // Sample data for the chart (you can replace this with your data from the backend)
    const chartData = {
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
        datasets: [
            {
                label: 'Sales',
                data: [12, 19, 3, 5, 2, 3, 7],
                borderColor: 'rgba(75, 192, 192, 1)',
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
            },
        ],
    };

    // Chart options
    const chartOptions = {
        scales: {
            y: {
                beginAtZero: true,
            },
        },
    };

    return (
        <div>
            <h2>Interactive Chart</h2>
            <Line data={chartData} options={chartOptions} />
        </div>
    );
};

export default Charts;
*/

// Charts.tsx

import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Line } from 'react-chartjs-2';

const Charts: React.FC = () => {
  // State to hold the fetched data
  const [chartData, setChartData] = useState({ labels: [], datasets: [] });

  // Fetch data from the backend
  useEffect(() => {
    axios.get('/api/data') // Replace '/api/data' with your backend API endpoint
      .then((response) => {
        setChartData(response.data); // Assuming the backend returns the chart data in the expected format
      })
      .catch((error) => {
        console.error('Error fetching data:', error);
      });
  }, []);

  // Chart options
  const chartOptions = {
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  };

  return (
    <div>
      <h2>Interactive Chart</h2>
      <Line data={chartData} options={chartOptions} />
    </div>
  );
};

export default Charts;
