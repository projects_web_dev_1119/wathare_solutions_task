"use strict";
exports.__esModule = true;
var express_1 = require("express");
var app = (0, express_1["default"])();
var port = 5500; // The port you want your server to run on
// Define your routes and middleware here
app.listen(port, function () {
    console.log("Server is running on http://localhost:".concat(port));
});
