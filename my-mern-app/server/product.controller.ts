// product.controller.ts

import { Request, Response } from 'express';
import Product from './product.model'; // Import the Product model interface or class


// product.controller.ts
/*
// Simulated in-memory data store (replace this with your actual database)
const products: Product[] = [];

// Function to create a new product
export const createProduct = (req: Request, res: Response) => {
    const { name, price, description, category } = req.body;

    const newProduct: Product = {
        _id: Math.random().toString(), // Replace this with a proper unique identifier (e.g., MongoDB's ObjectId)
        name,
        price,
        description,
        category,
        createdAt: new Date(),
        updatedAt: new Date(),
    };

    products.push(newProduct);

    res.status(201).json(newProduct);
};

// Function to get all products
export const getAllProducts = (req: Request, res: Response) => {
    res.json(products);
};


// product.controller.ts
*/

export default {
   // createProduct,
  //  getAllProducts,
};
