// product.model.ts

interface Product {
    _id: string;
    name: string;
    price: number;
    description: string;
    category: string;
    createdAt: Date;
    updatedAt: Date;
}

export default Product;
