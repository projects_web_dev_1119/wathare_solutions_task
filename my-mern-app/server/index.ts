import express from 'express';

const app = express();
const port = 5500; // The port you want your server to run on

// Define your routes and middleware here

app.listen(port, () => {
    console.log(`Server is running on http://localhost:${5500}`);
});
